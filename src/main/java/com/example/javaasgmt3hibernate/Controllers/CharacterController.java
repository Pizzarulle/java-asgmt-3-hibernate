package com.example.javaasgmt3hibernate.Controllers;

import com.example.javaasgmt3hibernate.CommonResponse;
import com.example.javaasgmt3hibernate.Models.Character;
import com.example.javaasgmt3hibernate.Services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/v1/character")
public class CharacterController {

    private final CharacterService characterService;

    @Autowired
    public CharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    /**
     * Returns all available characters
     * @return {@link ResponseEntity }<{@link List}<{@link Character}>>
     */
    @GetMapping("/all")
    public ResponseEntity<List<Character>> getAllCharacters(){
        return ResponseEntity.ok().body(characterService.getAllCharacters());
    }

    /**
     * Returns one character
     * @param id id of the caracter
     * @return {@link ResponseEntity }<{@link Character}>
     */
    @GetMapping(path = "/{characterId}")
    public ResponseEntity<Character> getCharacterById(@PathVariable("characterId") Long id)
    {
        return ResponseEntity.ok().body(characterService.getCharacterById(id));
    }

    /**
     * Adds a new character to the db
     * @param character new character-object
     * @return {@link ResponseEntity }
     */
    @PostMapping("/add")
    public ResponseEntity<?> addCharacter(@RequestBody Character character){
        Character addedCharacter = characterService.addCharacter(character);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/character/add").toUriString());
        return ResponseEntity.created(uri).body(addedCharacter);
    }

    /**
     * Removes a character by id
     * @param id id of the character
     * @return {@link ResponseEntity }
     */
    @DeleteMapping(path = "/delete/{characterId}")
    public ResponseEntity<Character> deleteCharacterById(@PathVariable("characterId") Long id){
        characterService.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    /**
     * Partially updates on a character. Only none relational fields can be patched
     * @param characterId id of existing character
     * @param patchedCharacter character object containing the new data
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PatchMapping("/patch/{characterId}")
    public ResponseEntity<CommonResponse> patchMovieById(@PathVariable long characterId, @RequestBody Character patchedCharacter){
        CommonResponse cr = new CommonResponse();

        if(characterService.existsById(characterId)){
            cr.setData(characterService.patchById(characterId, patchedCharacter));
            cr.setMessage("Character with id: " + characterId + " was patched");
        }
        else{
            cr.setMessage("Character not found with id: " + characterId);
        }
        return ResponseEntity.ok().body(cr);
    }
}
