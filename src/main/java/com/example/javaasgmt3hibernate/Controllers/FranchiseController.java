package com.example.javaasgmt3hibernate.Controllers;

import com.example.javaasgmt3hibernate.CommonResponse;
import com.example.javaasgmt3hibernate.Models.Franchise;
import com.example.javaasgmt3hibernate.Models.Movie;
import com.example.javaasgmt3hibernate.Services.FranchiseService;
import com.example.javaasgmt3hibernate.Services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/franchise")
public class FranchiseController {

    private final FranchiseService franchiseService;

    @Autowired
    public FranchiseController(FranchiseService franchiseService) {
        this.franchiseService = franchiseService;
    }

    /**
     * Returns all franchises
     *
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @GetMapping("/all")
    public ResponseEntity<CommonResponse> getAlL() {

        CommonResponse cr = new CommonResponse();
        cr.setData(franchiseService.getAll());
        cr.setMessage("All franchises");

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Get a specific franchise
     *
     * @param franchiseId id of franchise
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @GetMapping("/{franchiseId}")
    public ResponseEntity<CommonResponse> getById(@PathVariable long franchiseId) {

        CommonResponse cr = new CommonResponse();
        cr.setData(franchiseService.getById(franchiseId));
        cr.setMessage("Franchise with id " + franchiseId);

        if (cr.getData() == null) {
            cr.setMessage("No franchise with id: " + franchiseId + " found.");
        }

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Adds a new franchise
     *
     * @param franchise new franchise
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PostMapping("/add")
    public ResponseEntity<CommonResponse> addNewFranchise(@RequestBody Franchise franchise) {

        CommonResponse cr = new CommonResponse();
        cr.setData(franchiseService.save(franchise));
        cr.setMessage("Franchise saved to db.");

        if (cr.getData() == null) {
            cr.setMessage("Could not save franchise.");
        }

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Removes a franchise
     *
     * @param franchiseId id of franchise
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @DeleteMapping("/{franchiseId}")
    public ResponseEntity<CommonResponse> deleteById(@PathVariable long franchiseId) {

        CommonResponse cr = new CommonResponse();

        if (franchiseService.existsById(franchiseId)) {
            franchiseService.deleteById(franchiseId);
            cr.setMessage("Franchise with id: " + franchiseId + " was deleted");
        } else {
            cr.setMessage("Franchise not found with id: " + franchiseId);
        }

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Partially updates on a franchise. Only none relational fields can be patched
     *
     * @param franchiseId      id of franchise
     * @param partialFranchise franchise containing the new data
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PatchMapping("/{franchiseId}")
    public ResponseEntity<CommonResponse> patchById(@PathVariable long franchiseId, @RequestBody Franchise partialFranchise) {
        CommonResponse cr = new CommonResponse();

        if (franchiseService.existsById(franchiseId)) {
            cr.setData(franchiseService.patchById(franchiseId, partialFranchise));
            cr.setMessage("Franchise with id: " + franchiseId + " was patched");
        } else {
            cr.setMessage("Franchise not found with id: " + franchiseId);
        }

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Adds a new movie to a franchise
     *
     * @param franchiseId id of franchise
     * @param movie       new movie
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PostMapping("/{franchiseId}/addMovie")
    public ResponseEntity<CommonResponse> addMovieToFranchise(@PathVariable long franchiseId, @RequestBody Movie movie) {
        CommonResponse cr = new CommonResponse();

        if (franchiseService.existsById(franchiseId)) {
            cr.setData(franchiseService.addMovieToFranchise(franchiseId, movie));
            cr.setMessage("Movie was added to franchise with id: " + franchiseId);
        } else {
            cr.setMessage("Franchise not found with id: " + franchiseId);
        }

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Adds an existing movie to a franchise
     *
     * @param franchiseId id of franchise
     * @param movieId     id of movie
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PostMapping("/{franchiseId}/addMovie/{movieId}")
    public ResponseEntity<CommonResponse> addMovieById(@PathVariable long franchiseId, @PathVariable long movieId) {
        CommonResponse cr = new CommonResponse();

        if (franchiseService.existsById(franchiseId)) {
            Franchise franchise = franchiseService.addMovieToFranchise(franchiseId, movieId);
            if (franchise == null) {
                cr.setMessage("Movie with id: " + movieId + " was not found");
            } else {
                cr.setData(franchise);
                cr.setMessage("Movie with id: " + movieId + " was added to franchise with id: " + franchiseId);
            }
        } else {
            cr.setMessage("Franchise not found with id: " + franchiseId);
        }
        return ResponseEntity.ok().body(cr);
    }

    /**
     * Removes a movie from a franchise
     *
     * @param franchiseId id of franchise
     * @param movieId     id of movie to be removed
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PostMapping("{franchiseId}/removeMovie/{movieId}")
    public ResponseEntity<CommonResponse> removeMovieFromFranchiseById(@PathVariable long franchiseId, @PathVariable long movieId) {
        CommonResponse cr = new CommonResponse();

        if (franchiseService.existsById(franchiseId)) {
            Franchise franchise = franchiseService.removeMovieFromFranchiseById(franchiseId, movieId);
            if (franchise == null) {
                cr.setMessage("Movie with id: " + movieId + " was not found");
            } else {
                cr.setData(franchise);
                cr.setMessage("Movie with id: " + movieId + " was removed from franchise with id: " + franchiseId);
            }
        } else {
            cr.setMessage("Franchise not found with id: " + franchiseId);
        }
        return ResponseEntity.ok().body(cr);
    }

    /**
     * Returns all movies in a franchise
     * @param id id of franchise
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @GetMapping("/{franchiseId}/movies")
    public ResponseEntity<CommonResponse> getMoviesByFranchiseId(@PathVariable("franchiseId") Long id)
    {
        CommonResponse response = new CommonResponse();

        if(franchiseService.existsById(id)){
            response.setData(franchiseService.getMoviesByFranchiseId(id));
            response.setMessage("All movies included in franchise by id " + id);
        }
        else{
            response.setMessage("Franchise not found with id: " + id);
        }

        return ResponseEntity.ok().body(response);
    }

    /**
     * Returns all distinct characters in a franchise
     * @param id id of franchise
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @GetMapping("/{franchiseId}/characters")
    public ResponseEntity<CommonResponse> getCharactersByFranchiseId(@PathVariable("franchiseId") Long id){
        CommonResponse response = new CommonResponse();

        if(franchiseService.existsById(id)){
            response.setData(franchiseService.getCharactersByFranchiseId(id));
            response.setMessage("All characters included in franchise by id " + id);
        }
        else{
            response.setMessage("Franchise not found with id: " + id);
        }

        return ResponseEntity.ok().body(response);
    }
}
