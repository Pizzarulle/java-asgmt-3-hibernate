package com.example.javaasgmt3hibernate.Controllers;

import com.example.javaasgmt3hibernate.CommonResponse;
import com.example.javaasgmt3hibernate.Models.Character;
import com.example.javaasgmt3hibernate.Models.Movie;
import com.example.javaasgmt3hibernate.Services.FranchiseService;
import com.example.javaasgmt3hibernate.Services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RequestMapping("/api/v1/movie")
@RestController
public class MovieController {

    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService service) {
        this.movieService = service;
    }

    /**
     * Return all available movies
     * @return {@link ResponseEntity }<{@link List}<{@link Movie}>>
     */
    @GetMapping("/all")
    public ResponseEntity<List<Movie>> getAllMovies(){
        return ResponseEntity.ok().body(movieService.getAllMovies());
    }

    /**
     * Returns a specific movie
     * @param id id of the movie
     * @return {@link ResponseEntity }<{@link Movie}>
     */
    @GetMapping(path = "/{movieId}")
    public ResponseEntity<Movie> getMovieById(@PathVariable("movieId") Long id)
    {
        return ResponseEntity.ok().body(movieService.getMovieById(id));
    }

    /**
     * Adds a new movie to the db
     * @param movie new movie object
     * @return {@link ResponseEntity }<{@link Movie}>
     */
    @PostMapping("/add")
    public ResponseEntity<?> addMovie(@RequestBody Movie movie){

        movieService.addMovie(movie);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/movie/add").toUriString());
        return ResponseEntity.created(uri).body(movie);
    }

    /**
     * Removes a movie
     * @param id id of movie that will be removed
     * @return {@link ResponseEntity }
     */
    @DeleteMapping(path = "/delete/{movieId}")
    public ResponseEntity<Movie> deleteMovieById(@PathVariable("movieId") Long id){
        movieService.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    /**
     * Partially updates on a movie. Only none relational fields can be patched
     *
     * @param movieId id of the existing movie
     * @param patchedMovie new movie containing the new data
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PatchMapping("/patch/{movieId}")
    public ResponseEntity<CommonResponse> patchMovieById(@PathVariable long movieId, @RequestBody Movie patchedMovie){
        CommonResponse cr = new CommonResponse();

        if(movieService.existsById(movieId)){
            cr.setData(movieService.patchById(movieId, patchedMovie));
            cr.setMessage("Movie with id: " + movieId + " was patched");
        }
        else{
            cr.setMessage("Movie not found with id: " + movieId);
        }
    return ResponseEntity.ok().body(cr);
    }

    /**
     * Adds a new character to a movie
     * @param movieId id of the movie
     * @param character new character object
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PostMapping("/{movieId}/addCharacter")
    public ResponseEntity<CommonResponse> addCharacterToMovie(@PathVariable long movieId, @RequestBody Character character){
        CommonResponse cr = new CommonResponse();

        if(movieService.existsById(movieId)){
            cr.setData(movieService.addCharacterToMovie(movieId, character));
            cr.setMessage("Character was added to movie with id: " + movieId);
        }else {
            cr.setMessage("Movie not found with id: " + movieId);
        }

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Adds an existing character to a movie
     * @param movieId id of the movie
     * @param characterId id of the character that will be added
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PostMapping("/{movieId}/addCharacter/{characterId}")
    public ResponseEntity<CommonResponse> addCharacterToMovie(@PathVariable long movieId, @PathVariable long characterId){
        CommonResponse cr = new CommonResponse();

        if(movieService.existsById(movieId)){
            Movie movie = movieService.addCharacterToMovie(movieId, characterId);
            if(movie == null){
                cr.setMessage("Character with id: " + characterId + " was not found");
            }
            else{
                cr.setData(movie);
                cr.setMessage("Character with id:" + characterId + " was added to movie with id: " + movieId);
            }

        }else {
            cr.setMessage("Movie not found with id: " + movieId);
        }

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Removes an existing character from a movie
     * @param movieId id of the movie
     * @param characterId id of the character that will be removed
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @PostMapping("/{movieId}/removeCharacter/{characterId}")
    public ResponseEntity<CommonResponse> removeCharacterFromMovie(@PathVariable long movieId, @PathVariable long characterId){
        CommonResponse cr = new CommonResponse();
        if(movieService.existsById(movieId)){
            Movie movie = movieService.removeCharacterFromMovieById(movieId, characterId);
            if(movie == null){
                cr.setMessage("Character with id: " + characterId + " was not found");
            }
            else{
                cr.setData(movie);
                cr.setMessage("Character with id:" + characterId + " was added to movie with id: " + movieId);
            }
        }else {
            cr.setMessage("Movie not found with id: " + movieId);
        }

        return ResponseEntity.ok().body(cr);
    }

    /**
     * Returns all characters in a movie
     * @param id id of the movie
     * @return {@link ResponseEntity }<{@link CommonResponse}>
     */
    @GetMapping("/{movieId}/characters")
    public ResponseEntity<CommonResponse> getAllCharactersByMovieId(@PathVariable("movieId") Long id){
        CommonResponse response = new CommonResponse();

        if(movieService.existsById(id)){
            response.setData(movieService.getCharactersByMovieId(id));
            response.setMessage("All characters included in movie by id " + id);
        }
        else{
            response.setMessage("Movie not found with id: " + id);
        }

        return ResponseEntity.ok().body(response);
    }
}
