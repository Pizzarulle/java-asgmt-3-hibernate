package com.example.javaasgmt3hibernate.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Entity
@SQLDelete(sql = "update franchise_set set deleted = true where id =?")
@Where(clause = "deleted = false")
@Table(name = "franchise_set")
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @Column
    @JsonInclude(NON_NULL)
    private String description;

    @Column
    private boolean deleted = false;

    @JsonGetter("movies")
    public List<String> getMoviesList() {
        return movies.stream()
                .map(movie -> "/api/v1/movie/" + movie.getId())
                .collect(Collectors.toList());
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "franchise_id")
    @JsonInclude(NON_NULL)
    List<Movie> movies = new ArrayList<>();

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void addMovie(Movie movie) {
        movies.add(movie);
    }

    public void removeMovie(long movieId) {
        movies = movies.stream().filter(movie -> movie.getId() != movieId).collect(Collectors.toList());
    }
}
