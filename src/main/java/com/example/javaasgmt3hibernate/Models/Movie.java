package com.example.javaasgmt3hibernate.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Entity
@SQLDelete(sql = "update movie_set set deleted = true where id =?")
@Where(clause = "deleted = false")
@Table(name = "movie_set")

public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String title;

    @Column
    @JsonInclude(NON_NULL)
    private String genre;

    @Column
    @JsonInclude(NON_NULL)
    private String releaseYear;

    @Column
    @JsonInclude(NON_NULL)
    private String director;

    @Column
    @JsonInclude(NON_NULL)
    private String pictureUrl;

    @Column
    @JsonInclude(NON_NULL)
    private String trailerUrl;

    @Column
    private boolean deleted = false;

    @JsonGetter("franchise")
    public String getFranchiseLink() {
        if (franchise != null) {
            return "/api/v1/franchise/" + franchise.getId();
        }
        return null;
    }

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    @JsonInclude(NON_NULL)
    private Franchise franchise;

    @JsonGetter("characters")
    public List<String> getCharacterLink() {
        return characters.stream()
                .map(character -> "/api/v1/character/" + character.getId())
                .collect(Collectors.toList());
    }

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "character_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    @JsonInclude(NON_NULL)
    List<Character> characters = new ArrayList<>();

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }

    public void addCharacter(Character character) {
        this.characters.add(character);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void removeCharacter(long characterId) {
        characters = characters.stream().filter(character -> character.getId() != characterId).collect(Collectors.toList());
    }
}
