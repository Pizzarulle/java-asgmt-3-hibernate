package com.example.javaasgmt3hibernate.Models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Entity
@SQLDelete(sql = "update character_set set deleted = true where id =?")
@Where(clause = "deleted = false")
@Table(name = "character_set")
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String fullName;

    @Column
    @JsonInclude(NON_NULL)
    private String alias;

    @Column
    @JsonInclude(NON_NULL)
    private String gender;

    @Column
    @JsonInclude(NON_NULL)
    private String pictureUrl;

    @Column
    private boolean deleted = false;

    @JsonGetter("movies")
    public List<String> getMovieLink() {
        return movies.stream()
                .map(movie1 -> "/api/v1/movie/" + movie1.getId())
                .collect(Collectors.toList());
    }

    @ManyToMany(mappedBy = "characters", cascade = CascadeType.ALL)
    @JsonInclude(NON_NULL)
    private List<Movie> movies = new ArrayList<>();

    public long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movie) {
        this.movies = movie;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void addMovie(Movie movie) {
        this.movies.add(movie);
    }
}
