package com.example.javaasgmt3hibernate;

import com.example.javaasgmt3hibernate.Models.Character;
import com.example.javaasgmt3hibernate.Models.Franchise;
import com.example.javaasgmt3hibernate.Models.Movie;
import com.example.javaasgmt3hibernate.Repositories.CharacterRepository;
import com.example.javaasgmt3hibernate.Repositories.FranchiseRepository;
import com.example.javaasgmt3hibernate.Repositories.MovieRepository;
import com.example.javaasgmt3hibernate.Services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class JavaAsgmt3HibernateApplication {

    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    CharacterRepository characterRepository;

    public static void main(String[] args) {
        SpringApplication.run(JavaAsgmt3HibernateApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        return args -> {
            initStarWars();
            initLordOfTheRings();
            initIndianaJones();
        };
    }


    // Man måste spara character och sedan lägga till dom i movie. Det sammma med movices och franchises.
    //Denna fungerar men dessa steg behöver göras på de andra metoderna.
    public void initSingleMovie() {
        Movie d9 = new Movie();
        d9.setTitle("Disctrict 9");

        movieRepository.save(d9);

        Character bugg = new Character();
        bugg.setFullName("Bugg alien");
        bugg.setAlias("alien bugg boi");
        characterRepository.save(bugg);

        Character mainDude = new Character();
        mainDude.setAlias("Main Dude");
        mainDude.setGender("m/Alien");
        characterRepository.save(mainDude);

        d9.addCharacter(bugg);
        d9.addCharacter(mainDude);
    }

    public void initStarWars() {
        Franchise f = new Franchise();
        f.setName("Star wars");
        f.setDescription("Story of a war in space.");
        franchiseRepository.save(f);

        Movie esb = new Movie();
        esb.setTitle("Empire strikes back");
        esb.setFranchise(f);
        esb.setDirector("Irvin Kershner");
        esb.setGenre("Science Fiction");
        esb.setReleaseYear("1980");
        movieRepository.save(esb);

        Character luke = new Character();
        luke.setAlias("Luke");
        luke.setGender("m");
        luke.setFullName("Luke Skywalker");

        Character leia = new Character();
        leia.setAlias("Leia");
        leia.setGender("f");
        leia.setFullName("Leia organa");
//        leia.addMovie(esb);

        Character han = new Character();
        han.setAlias("Solo");
        han.setGender("m");
        han.setFullName("Han Solo");
//        han.addMovie(esb);

        List<Character> cars = new ArrayList<>();
        cars.add(luke);
        cars.add(leia);
        cars.add(han);

        esb.setCharacters(cars);
        movieRepository.save(esb);
    }

    public void initLordOfTheRings() {
        Franchise f = new Franchise();
        f.setName("Lord Of The Rings");
        f.setDescription("A tale of a ring and a hobbit!");
        franchiseRepository.save(f);

        Movie esb = new Movie();
        esb.setTitle("The Lord Of The Rings: The Two Towers");
        esb.setFranchise(f);
        esb.setDirector("Peter Jackson");
        esb.setGenre("Fantasy");
        esb.setReleaseYear("2002");
        movieRepository.save(esb);

        Character sam = new Character();
        sam.setAlias("Sam");
        sam.setGender("male");
        sam.setFullName("Samwise Gamgee");

        Character frodo = new Character();
        frodo.setAlias("Frodo");
        frodo.setGender("male");
        frodo.setFullName("Frodo Baggins");


        List<Character> cars = new ArrayList<>();
        cars.add(frodo);
        cars.add(sam);
        esb.setCharacters(cars);

        movieRepository.save(esb);
    }

    public void initIndianaJones() {
        Franchise f = new Franchise();
        f.setName("Indiana Jones");
        f.setDescription("A archeologist embarks on different adventures");
        franchiseRepository.save(f);

        Movie esb = new Movie();
        esb.setTitle("Raiders Of The Lost Ark");
        esb.setFranchise(f);
        esb.setDirector("Steven Spielberg");
        esb.setGenre("Adventure");
        esb.setReleaseYear("1981");
        movieRepository.save(esb);

        Character indiana = new Character();
        indiana.setAlias("Indy");
        indiana.setGender("male");
        indiana.setFullName("Indiana Jones");


        List<Character> cars = new ArrayList<>();
        cars.add(indiana);
        esb.setCharacters(cars);

        movieRepository.save(esb);
    }
}

