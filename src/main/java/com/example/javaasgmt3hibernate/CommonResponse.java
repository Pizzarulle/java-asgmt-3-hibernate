package com.example.javaasgmt3hibernate;

public class CommonResponse {
    private Object data;
    private String message;

    public CommonResponse(){}

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
