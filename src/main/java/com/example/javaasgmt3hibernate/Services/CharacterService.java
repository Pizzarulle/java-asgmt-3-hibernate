package com.example.javaasgmt3hibernate.Services;

import com.example.javaasgmt3hibernate.Models.Character;
import com.example.javaasgmt3hibernate.Models.Movie;
import com.example.javaasgmt3hibernate.Repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CharacterService {
    private final CharacterRepository characterRepository;
    private final MovieService movieService;

    @Autowired
    public CharacterService(CharacterRepository characterRepository, MovieService movieService) {
        this.characterRepository = characterRepository;
        this.movieService = movieService;
    }

    public List<Character> getAllCharacters() {
        return characterRepository.findAll();
    }

    public Character getCharacterById(Long id) {
        Optional<Character> optionalCharacter = characterRepository.findById(id);

        if (!optionalCharacter.isPresent())
            throw new IllegalStateException(String.format(Long.toString(id)));

        return optionalCharacter.get();
    }

    public Character addCharacter(Character character) {
        characterRepository.save(character);
        List<Movie> col = character.getMovies().stream().map(movie -> {
            Movie addMovie = movieService.getMovieById(movie.getId());
            addMovie.addCharacter(character);

            return addMovie;
        }).collect(Collectors.toList());

        col.forEach(movie -> {
            movieService.addMovie(movie);
        });

        return character;
    }

    public void deleteById(Long id) {
        characterRepository.deleteById(id);
    }


    public Character patchById(long id, Character partialCharacter) {

        Character currentCharacter = getCharacterById(id);

        if (partialCharacter.getFullName() != null) {
            currentCharacter.setFullName(partialCharacter.getFullName());
        }
        if (partialCharacter.getAlias() != null) {
            currentCharacter.setAlias(partialCharacter.getAlias());
        }
        if (partialCharacter.getGender() != null) {
            currentCharacter.setGender(partialCharacter.getGender());
        }
        if (partialCharacter.getPictureUrl() != null) {
            currentCharacter.setPictureUrl(partialCharacter.getPictureUrl());
        }

        return characterRepository.save(currentCharacter);
    }

    public boolean existsById(long id) {
        return characterRepository.existsById(id);
    }
}
