package com.example.javaasgmt3hibernate.Services;

import com.example.javaasgmt3hibernate.Models.Character;
import com.example.javaasgmt3hibernate.Models.Movie;
import com.example.javaasgmt3hibernate.Repositories.CharacterRepository;
import com.example.javaasgmt3hibernate.Repositories.FranchiseRepository;
import com.example.javaasgmt3hibernate.Repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MovieService {
    private final MovieRepository movieRepository;
    private final FranchiseRepository franchiseRepository;
    private final CharacterRepository characterRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository, FranchiseRepository franchiseRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
        this.characterRepository = characterRepository;
    }

    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    /**
     * Returns all movies
     * @return List of movies
     */
    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    public Movie getMovieById(Long id) {

        Optional<Movie> movieOptional = movieRepository.findById(id);

        if (!movieOptional.isPresent()) {
            throw new IllegalStateException(String.format(Long.toString(id)));
        }

        return movieOptional.get();
    }

    /**
     * Saves a movie
     * @param movie movie that will be saved
     */
    public void addMovie(Movie movie) {
        if (movie.getFranchise() != null) {
            if (movie.getFranchise().getId() != 0) {
                movie.setFranchise(franchiseRepository.getById(movie.getFranchise().getId()));
            } else {
                franchiseRepository.save(movie.getFranchise());
            }
        }

        movieRepository.save(movie);
    }

    /**
     * Deletes a movie by its id
     * @param id id of the movie
     */
    public void deleteById(Long id) {
        movieRepository.deleteById(id);
    }

    /**
     * Checks if a movie exists in the db
     * @param id id of the movie
     * @return true/false
     */
    public boolean existsById(long id) {
        return movieRepository.existsById(id);
    }

    /**
     * Updates the none relational fields of an existing movie
     * @param id id of existing movie
     * @param partialMovie movie containing the new data
     * @return updated and saved movie
     */
    public Movie patchById(long id, Movie partialMovie) {

        System.out.println(partialMovie);
        Movie currentMovie = getMovieById(id);

        if (partialMovie.getTitle() != null) {
            currentMovie.setTitle(partialMovie.getTitle());
        }
        if (partialMovie.getGenre() != null) {
            currentMovie.setGenre(partialMovie.getGenre());
        }
        if (partialMovie.getReleaseYear() != null) {
            currentMovie.setReleaseYear(partialMovie.getReleaseYear());
        }
        if (partialMovie.getDirector() != null) {
            currentMovie.setDirector(partialMovie.getDirector());
        }
        if (partialMovie.getDirector() != null) {
            currentMovie.setDirector(partialMovie.getDirector());
        }
        if (partialMovie.getPictureUrl() != null) {
            currentMovie.setPictureUrl(partialMovie.getPictureUrl());
        }
        if (partialMovie.getTrailerUrl() != null) {
            currentMovie.setTrailerUrl(partialMovie.getTrailerUrl());
        }

        addMovie(currentMovie);
        return currentMovie;

    }

    public Optional<Movie> findById(long id) {
        return movieRepository.findById(id);
    }

    /**
     * Adds a new character to a movie
     * @param movieId id of the movie
     * @param character new character that will be added
     * @return the updated movie containing the added movie
     */
    public Movie addCharacterToMovie(long movieId, Character character) {
        Movie movie = getMovieById(movieId);
        movie.addCharacter(character);
        return movieRepository.save(movie);
    }

    /**
     * Adds an existing character to a movie
     * @param movieId id of the movie
     * @param characterId id of the existing character
     * @return the updated movie containing the movie
     */
    public Movie addCharacterToMovie(long movieId, long characterId) {
        Movie movie = getMovieById(movieId);
        if (characterRepository.existsById(movieId)) {
            if (movie.getCharacters().stream().noneMatch(character -> character.getId() == characterId)) {
                movie.addCharacter(characterRepository.getById(characterId));
                return movieRepository.save(movie);
            }
        }
        return null;
    }


    /**
     * Removes a character from a movie
     * @param movieId id of movie
     * @param characterId id of character that will be removed from the movie
     * @return updated movie without the that character
     */
    public Movie removeCharacterFromMovieById(long movieId, long characterId) {
        Movie movie = movieRepository.getById(movieId);
        if (characterRepository.existsById(characterId)) {
            movie.removeCharacter(characterId);
            return movieRepository.save(movie);
        }
        return null;
    }

    /**
     * Returns one character on a movie
     * @param id id of the movie
     * @return character or throw error
     */
    public Object getCharactersByMovieId(Long id) {
        Optional<Movie> optionalMovie = movieRepository.findById(id);
        List<Character> characters = new ArrayList<>();
        if(optionalMovie.isPresent()){
            characters.addAll(optionalMovie.get().getCharacters());
        }
        else {
            throw new IllegalStateException("Franchise by id of %s does not exist".format(Long.toString(id)));
        }
        return characters;
    }
}
