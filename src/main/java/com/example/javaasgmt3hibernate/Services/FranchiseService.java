package com.example.javaasgmt3hibernate.Services;

import com.example.javaasgmt3hibernate.Models.Character;
import com.example.javaasgmt3hibernate.Models.Franchise;
import com.example.javaasgmt3hibernate.Models.Movie;
import com.example.javaasgmt3hibernate.Repositories.FranchiseRepository;
import com.example.javaasgmt3hibernate.Repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FranchiseService {
    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    MovieRepository movieRepository;

    public List<Franchise> getAll() {
        return franchiseRepository.findAll();
    }

    public Franchise getById(long id) {
        Optional<Franchise> franchise = franchiseRepository.findById(id);
        return franchise.orElse(null);
    }

    /**
     * Saves a franchise to the db
     * @param franchise - can not be null
     * @return
     */
    public Franchise save(Franchise franchise) {
        if (franchise.getMovies().size() > 0) {
            franchise.getMovies().forEach(movie -> {
                if (movie.getId() == 0) {
                    movieRepository.save(movie);
                }
            });
        }

        return franchiseRepository.save(franchise);
    }

    /**
     * Deletes a franchise from db
     * @param id franchise id
     */
    public void deleteById(long id) {
        franchiseRepository.deleteById(id);
    }

    /**
     * Updates the none relational fields of an existing franchise
     * @param id Franchise id
     * @param partialFranchise - franchise containing the new data
     * @return updated franchise
     */
    public Franchise patchById(long id, Franchise partialFranchise) {

        Franchise currentFranchise = getById(id);

        if (partialFranchise.getDescription() != null) {
            currentFranchise.setDescription(partialFranchise.getDescription());
        }
        if (partialFranchise.getName() != null) {
            currentFranchise.setName(partialFranchise.getName());
        }
        return save(currentFranchise);
    }

    /**
     * Checks if a franchise exists in the db
     * @param id franchise id
     * @return true/false
     */
    public boolean existsById(long id) {
        return franchiseRepository.existsById(id);
    }

    /**
     * Adds a new movie to a franchise
     * @param franchiseId id of franchise
     * @param movie new movie that will be added to the franchise
     * @return updated franchise containing the added movie
     */
    public Franchise addMovieToFranchise(long franchiseId, Movie movie) {
        Franchise franchise = getById(franchiseId);
        franchise.addMovie(movie);
        return franchiseRepository.save(franchise);
    }

    /**
     * Adds an existing movie to a franchise
     * @param franchiseId id of franchise
     * @param movieId id of movie
     * @return updated franchise containing the added movie
     */
    public Franchise addMovieToFranchise(long franchiseId, long movieId) {
        Franchise franchise = getById(franchiseId);
        if (movieRepository.existsById(movieId)) {
            if (franchise.getMovies().stream().noneMatch(movie -> movie.getId() == movieId)) {
                franchise.addMovie(movieRepository.getById(movieId));
                return franchiseRepository.save(franchise);
            }
        }
        return null;
    }

    /**
     * Removes a movie from a franchise
     * @param franchiseId id of franchise
     * @param movieId id of movie to be removed
     * @return update franchise without the specified movie
     */
    public Franchise removeMovieFromFranchiseById(long franchiseId, long movieId) {
        Franchise franchise = getById(franchiseId);
        if (movieRepository.existsById(movieId)) {
            franchise.removeMovie(movieId);
            return franchiseRepository.save(franchise);
        }
        return null;
    }

    /**
     * Returns all movies in a franchise
     * @param id - franchise id
     * @return collection of movies
     */
    public Collection<Movie> getMoviesByFranchiseId(Long id){
        Optional<Franchise> optionalFranchise = franchiseRepository.findById(id);
        List<Movie> movies = new ArrayList<>();
        if(optionalFranchise.isPresent()){
            movies.addAll(optionalFranchise.get().getMovies());
        }
        else {
            throw new IllegalStateException("Franchise by id of %s does not exist".format(Long.toString(id)));
        }
        return movies;
    }

    /**
     * Returns all distinct characters in a franchise
     * @param id franchise id
     * @return collection of characters
     */
    public Collection<Character> getCharactersByFranchiseId(Long id) {
        Map<Long,Character> allCharactersInFranchise = new HashMap<>();
        Optional<Franchise> optionalFranchise = franchiseRepository.findById(id);

        if(optionalFranchise.isPresent()){
            Collection<Movie> movies = optionalFranchise.get().getMovies();

            for(Movie movie :movies){
                for(Character character :movie.getCharacters())
                {
                    allCharactersInFranchise.put(character.getId(),character);
                }
            }

            return allCharactersInFranchise.values();
        }
        else {
            throw new IllegalStateException("Franchise by id of %s does not exist".format(Long.toString(id)));
        }
    }
}
