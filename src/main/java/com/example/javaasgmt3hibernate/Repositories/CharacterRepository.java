package com.example.javaasgmt3hibernate.Repositories;

import com.example.javaasgmt3hibernate.Models.Character;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CharacterRepository extends JpaRepository<Character, Long> {
}
