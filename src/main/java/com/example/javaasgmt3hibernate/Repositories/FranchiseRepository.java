package com.example.javaasgmt3hibernate.Repositories;

import com.example.javaasgmt3hibernate.Models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FranchiseRepository extends JpaRepository<Franchise, Long> {

}
