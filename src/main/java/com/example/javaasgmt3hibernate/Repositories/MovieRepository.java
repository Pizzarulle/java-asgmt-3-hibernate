package com.example.javaasgmt3hibernate.Repositories;

import com.example.javaasgmt3hibernate.Models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MovieRepository extends JpaRepository<Movie, Long> {
}
