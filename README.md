# Api for handling franchises, movies and characters in a database

A project created with the aim of learing spring hibernate together with a postgreSQL database.
This api contains full CRUD endpoints for franchises,movies and characters.

## Demo

https://hibernate-app-2.herokuapp.com/api/v1/movie/all

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/Pizzarulle/java-asgmt-3-hibernate.git
```


Start the application

```bash
  Run the file JavaAsgmt3HibernateApplication to start the server.
```



## Test

To test the api's endpoints

```bash
  navigate to http://localhost:8080/swagger-ui/index.html in your preffered browser.
  Here you can test the different endpoints.
```

Note for patch endpoints

```bash
  The patch endpoints can only update the data on entities that has no relationalmapping.
```
## Add / remove  movies to franchise
```
POST /api/v1/franchise/:franchiseId/addMovie
POST /api/v1/franchise/:franchiseId/addMovie/:movieId
POST /api/v1/franchise/:franchiseId/removeMovie/:movieId
```
## Add / remove character to movie
```
POST /api/v1/movie/:movieId/addCharacter
POST /api/v1/movie/:movieId/addCharacter/:characterId
POST /api/v1/movie/:movieId/removeCharacter/:characterId
```

## Authors

- [@Pizzarulle](https://gitlab.com/Pizzarulle)
- [@mikellove](https://gitlab.com/mikaellove)